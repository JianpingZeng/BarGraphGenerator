import os
import re
from enum import Enum
from matplotlib.hatch import Shapes
from matplotlib.patches import Rectangle
from matplotlib.hatch import _hatch_types

# This enumerate class is aimed at providing discrimination between
# different kind of bar graph to be drawn, such as simple bar, cluster,
# stacked bar or cluster stacked bar.
class GraphKind(Enum):
    SimpleBar = 1
    ClusterBar = 2
    StackedBar = 3
    StackedClusterBar = 4
    MirrorClusterBar = 5
    LineChart = 6

# This class is used to encapsulate properties of extraops option
class ExtraOps:
    def __init__(self, label, label_loc, font_name, font_size):
        self.label = label
        self.label_loc = label_loc
        self.font_name = font_name
        self.font_size = font_size

class SquareHatch(Shapes):
    """
    Square hatch defined by a path drawn inside [-0.5, 0.5] square.
    Identifier 's'.
    """
    def __init__(self, hatch, density):
        self.filled = False
        self.size = 1
        self.path = Rectangle((-0.25, 0.25), 0.5, 0.5).get_path()
        self.num_rows = hatch.count('d') * 2
        self.shape_vertices = self.path.vertices
        self.shape_codes = self.path.codes
        Shapes.__init__(self, hatch, density)

# attach our new hatch
_hatch_types.append(SquareHatch)

class GraphObject:
    def __init__(self, output="-", language='pdf'):
        # The default output is standard output file when user don't explicitly specify it.
        self.output = output
        self.language = language
        self.harmean = False
        self.geomean = False
        self.arithmean = False
        self.meanlabel = None
        self.sortmarks = False
        self.yformat = None
        self.xlabel = None
        self.ylabel = None
        self.norotate = False
        self.xscale = None
        self.ymax = None
        self.ymin = None

        self.right_yformat = None
        self.right_ylabel = None
        self.right_ymax = None
        self.right_ymin = None

        # Here we should use the default colors to fill the bars if the user don't specify it
        self.colors = ['black', 'darkgreen','yellow', 'red', 'blue','cyan',
                       'whitesmoke', 'mediumblue', 'grey', 'magenta', 'lightblue', 'lightgreen']
        # a list of hatches for bars or markers for line chart
        # '/', '\', '|', '-', '+', 'x', 'o', 'O', '.', '*'
        # https://matplotlib.org/stable/api/markers_api.html
        self.hatches = {}
        # the following option is only used for line chart.
        self.markersize = 10

        # the following option is only used for line chart.
        self.linewidth = 1.5

        self.is_table = False
        # The default delimiter between multiple elements in a table is space unless user explicitly specify it.
        self.delimiter = ' '

        # The default kind of graph to be drawn is simple bar.
        self.graphKind = GraphKind.SimpleBar
        # The data table in which the data read from external file will be filled.
        self.table = []
        self.grouplabels = []
        self.grouplabel_font = 14
        self.supergrouplabels = []
        self.supergrouplabel_font = 22

        self.blank = None
        # this list saves those label placed for those bars have overflow value
        self.extraops = None
        self.fig_width = 16
        self.fig_height = 9        
        self.label_size = 8
        # x_label_size is a alias to label_size
        self.x_label_size = self.label_size
        self.y_label_size = 8

        # the following option is used to specify the times of bar width
        # will be used to fill the margin between bars.
        self.bar_margin = 1.05
        # specify the margin between groups of bars in order of bar width.
        self.group_margin = 1

        # this option is used to determine whether we should place a value label on the top of each bar.
        self.value_label_top_bar = False
        self.value_label_fontsize = 5
        self.horizontal_legend = False
        self.fontsize = 22
        self.legend_loc = None
        self.group_rotateby = 0

        # Control how many columns in legend.
        self.ncol = None
        self.rotateby = 90
        self.over_top_legend = False
        self.left_outside_legend = False
        self.grid_linewidth = 3

        # following two options list the length of segment in points on/off the ink for the dashed line.
        self.onink = 4
        self.offink = 8

        # Control number of bins places in either x axis or y axis
        self.xnbins = None
        self.ynbins = None
        self.no_xticks_label = False
        self.y_log_scale = False
        self.mirror_y_axis = False

        self.source_scale = None
        self.destination_scale = None
        self.y_tick_labels = None

    def read_data_section(self, line):
        assert len(line) > 0, "empty string shouldn't passed into read_data_section function"
        # the splited items of the line is looks like the following.
        # "ammp"  "15.07"
        # the first element of each line represents the application name or configuration name.
        # the second element means its value or weight.
        temp = line.split(self.delimiter)
        temp = [val for val in temp if len(val) > 0]
        self.table.append(list(map(str.strip, temp)))

    def parse_input_file(self, input):
        assert os.path.exists(input), "the input file must exists"
        with open(input, "r") as f:
            lines = f.readlines()
            pattern = re.compile(".+=.+")

            num_lines = len(lines)
            row_num = 0
            # The reason why we use the following method to iterate over the lines is
            # we need to extract data part when entering the data section.
            while row_num < num_lines:
                line = lines[row_num].strip()
                if len(line) <= 0:
                    row_num += 1
                    continue
                if (line.startswith("=")):
                    # Handle the boolean option, such as =barmean, =sortmarks etc.
                    # strip the leaning '=' sign
                    option = line[1:]

                    if option.startswith("harmean"):
                        self.harmean = True
                    elif option.startswith("arithmean"):
                        self.arithmean = True
                    elif option.startswith("geomean"):
                        self.geomean = True
                    elif option.startswith("sortbmarks"):
                        self.sortmarks = True
                    elif option.startswith("cluster"):
                        # handle the cluster bar graph
                        # Indicates that there are multiple datasets that should be displayed as clustered bars.
                        # This command also provides the names of the datasets. The character following =cluster is
                        # taken as a delimiter separating the rest of the line into strings naming each dataset.
                        # Some examples:
                        #      =cluster;Irish elk;Dodo birds;Coelecanth
                        #      =cluster Monday Tuesday Wednesday Thursday Friday
                        #      =cluster+Fast; slow+Slow; fast
                        self.graphKind = GraphKind.ClusterBar
                        if len(option) > len("cluster")+1:
                            option = option[len("cluster"):]
                            delimiter = option[0]
                            # strip of the delimiter
                            option = option[1:]
                            args = option.split(delimiter)
                            self.column_headers = args
                        else:
                            self.column_headers = [""]
                    elif option.startswith("mirrorCluster"):
                        self.graphKind = GraphKind.MirrorClusterBar
                        if len(option) > len("mirrorCluster")+1:
                            option = option[len("mirrorCluster"):]
                            delimiter = option[0]
                            # strip of the delimiter
                            option = option[1:]
                            args = option.split(delimiter)
                            self.column_headers = args
                        else:
                            self.column_headers = [""]
                    elif option.startswith("table"):
                        self.is_table = True
                        # specify the data will be listed in columns.
                        if len(option) > len("table"):
                            # Reach here, which means there is a explicit delimiter
                            self.delimiter = option[len("table"):]

                        # Starts a data section
                        row_num += 1
                        break
                    elif option.startswith("stackedcluster"):
                        self.graphKind = GraphKind.StackedClusterBar
                        if len(option) > len("stackedcluster")+1:
                            option = option[len("stackedcluster"):]
                            delimiter = option[0]
                            # strip of the delimiter
                            option = option[1:]
                            args = option.split(delimiter)
                            self.column_headers = args
                        else:
                            self.column_headers = [""]

                    elif option.startswith("stacked"):
                        self.graphKind = GraphKind.StackedBar
                        if len(option) > len("stacked")+1:
                            option = option[len("stacked"):]
                            delimiter = option[0]
                            # strip of the delimiter
                            option = option[1:]
                            args = option.split(delimiter)
                            self.column_headers = args
                        else:
                            self.column_headers = [""]
                    elif option.startswith("line"):
                        self.graphKind = GraphKind.LineChart
                        if len(option) > len("line") + 1:
                            option = option[len("line"):]
                            delimiter = option[0]
                            # strip of the delimiter
                            option = option[1:]
                            args = option.split(delimiter)
                            self.column_headers = args

                    elif option.startswith("norotate"):
                        self.norotate = True
                    elif option == "horizontal_legend":
                        self.horizontal_legend = True
                    elif option == "valuelabel":
                        self.value_label_top_bar = True
                    elif option == "over_top_legend":
                        self.over_top_legend = True
                    elif option == "left_outside_legend":
                        self.left_outside_legend = True
                    elif option == "no_xticks_label":
                        self.no_xticks_label = True
                    elif option == "y_log_scale":
                        self.y_log_scale = True
                    elif option == "mirror_y_axis":
                        self.mirror_y_axis = True
                elif line.startswith("#"):
                    # skip those comment line
                    pass
                elif pattern.match(line):
                    # Handle those value arguments
                    tmp_arrays = line.split("=")
                    arg = tmp_arrays[0]
                    value = tmp_arrays[1]
                    if arg == "meanlabel":
                        self.meanlabel = value
                    elif arg == "xlabel":
                        self.xlabel = value
                    elif arg == "ylabel":
                        self.ylabel = value.replace("\\n", "\n")
                    elif arg == "yformat":
                        self.yformat = value
                    elif arg == "colors":
                        self.colors = []
                        # This sequence of colors are used to mark the different bar of same group.
                        temp = value.split(',')
                        i = 0
                        for c in temp:
                            if c.count(':') != 0:
                                color_pattern = c.split(':')
                                self.colors.append(color_pattern[0])
                                self.hatches[i] = color_pattern[1]
                            else:
                                self.colors.append(c)
                            i += 1
                    elif arg == "max" or arg == "ymax":
                        self.ymax = float(value)
                    elif arg == "neg_max" or arg == "ymin":
                        self.ymin = float(value)
                    elif arg == "right_yformat":
                        self.right_yformat = value
                    elif arg == "right_ylabel":
                        self.right_ylabel = value.replace("\\n", "\n")
                    elif arg == "right_ymax":
                        self.right_ymax = float(value)
                    elif arg == "right_ymin":
                        self.right_ymin = float(value)
                    elif arg == "xscale":
                        self.xscale = float(value)
                    elif arg == "rotateby":
                        #if not value.isdigit() :
                        #    print("error: only number can be used to specify what degree a bar would be rotated in line {0}".format(row_num))
                        self.rotateby = int(value)
                    elif arg == "grouplabels":
                        # res = []
                        # temp = value.split(';')
                        # for t in temp:
                        #     items = t.split(':')
                        #     label = items[0]
                        #     start_index = int(items[1].split(',')[0])
                        #     end_index = int(items[1].split(',')[1])
                        #     res.append([label, start_index, end_index])
                        # self.grouplabels = res
                        # Ignore
                        pass
                    elif arg == "supergrouplabels":
                        # res = []
                        # temp = value.split(';')
                        # for t in temp:
                        #     items = t.split(':')
                        #     label = items[0]
                        #     start_index = int(items[1].split(',')[0])
                        #     end_index = int(items[1].split(',')[1])
                        #     res.append([label, start_index, end_index])
                        # self.supergrouplabels = res
                        # Ignore
                        pass
                    elif arg == "blank":
                        # if not ',' in value:
                        #     self.blank = []
                        #     self.blank.append(int(value))
                        # else:
                        #     self.blank = list(map(int, value.split(',')))
                        #     self.blank.sort()
                        pass
                    elif arg == "bar_margin":
                        self.bar_margin = float(value)
                    elif arg == "group_margin":
                        self.group_margin = float(value)
                    elif arg == "extraops":
                        # extraops=set label "3.36" at 3,6 font "Times,10"

                        if not value.startswith("set label "):
                            print("mal-formed set value of extraops in line {0}".format(row_num + 1))
                            return False
                        value = value[len("set label "):]
                        if not value.startswith('"'):
                            print("mal-formed set value of extraops in line {0}".format(row_num + 1))
                            return False

                        j = 1
                        sz = len(value)
                        label = ''
                        while j < sz and value[j] != '"':
                            label += value[j]
                            j += 1
                        if j >= sz:
                            print("mal-formed label of extraops, no closed \" in line {0}".format(row_num + 1))
                            return False


                        # strip off the leading label and space
                        value = value[j+1:].strip()
                        if not value.startswith("at "):
                            print("mal-formed label location of extraops in line {0}".format(row_num + 1))
                            return False
                        value = value[len("at "):]
                        j = 0
                        sz = len(value)
                        while j < sz and (str.isdigit(value[j]) or value[j]==','):
                            j += 1

                        if j >= sz:
                            print("mal-formed label location of extraops in line {0}".format(row_num + 1))
                            return False
                        location = value[0:j]
                        label_loc = []

                        if ',' in location:
                            temp = location.split(',')
                            if len(temp) != 2:
                                print("mal-formed label location of extraops in line {0}".format(row_num + 1))
                                return False
                            label_loc.append(int(temp[0]))
                            label_loc.append(int(temp[1]))
                        else:
                            label_loc.append([int(location), None])

                        value = value[j+1:]
                        if not value.startswith("font "):
                            print("mal-formed font of extraops in line {0}".format(row_num + 1))
                            return False
                        value = value[len("font "):]
                        j = 1
                        sz = len(value)
                        font_name_size = ''
                        while j < sz and value[j] != '"':
                            font_name_size += value[j]
                            j += 1
                        if j >= sz:
                            print("mal-formed font of extraops, no closed \" in line {0}".format(row_num + 1))
                            return False

                        # extract font name and font size from the font_name_size string
                        if not ',' in font_name_size:
                            print("mal-formed font of extraops, no ',' to delimite font string in line {0}".format(row_num + 1))
                            return False
                        temp = font_name_size.split(',')
                        font_name = temp[0].strip()
                        font_size = int(temp[1].strip())

                        if self.extraops == None:
                            self.extraops = []
                        self.extraops.append(ExtraOps(label, label_loc, font_name, font_size))

                    elif arg == "groupfont":
                        self.grouplabel_font = int(value)
                    elif arg == 'supergroupfont':
                        self.supergrouplabel_font = int(value)
                    elif arg == "figsize":
                        # use the option to specify the width and height of the generated bar graph.
                        temp = value.split(',')
                        self.fig_width = float(temp[0])
                        self.fig_height = float(temp[1])
                    elif arg == "labelsize" or arg == "x_label_size":
                        self.label_size = int(value)
                        self.x_label_size = int(value)
                    elif arg == "y_label_size":
                        self.y_label_size = int(value)
                    elif arg == "fontsize":
                        self.fontsize = int(value)
                    elif arg == "group_offset":
                        self.group_offset = float(value)
                    elif arg == "super_group_offset":
                        self.super_group_offset = float(value)
                    elif arg == "legend_loc":
                        self.legend_loc = str(value)
                    elif arg == "group_rotateby":
                        self.group_rotateby = int(value)
                    elif arg == "ncol":
                        self.ncol = int(value)
                    elif arg == "grid_linewidth":
                        self.grid_linewidth = float(value)
                    elif arg == "onink":
                        self.onink = float(value)
                    elif arg == "offink":
                        self.offink = float(value)
                    elif arg == "ynbins":
                        self.ynbins = int(value)
                    elif arg == "xnbins":
                        self.xnbins = int(value)
                    elif arg == "markersize":
                        self.markersize = int(value)
                    elif arg == "source_scale":
                        for item in value.split(";"):
                            temp = item.split(",")
                            if self.source_scale == None:
                                self.source_scale = []
                            self.source_scale.append((float(temp[0]), float(temp[1])))
                    elif arg == "destination_scale":
                        for item in value.split(";"):
                            temp = item.split(",")
                            if self.destination_scale == None:
                                self.destination_scale = []
                            self.destination_scale.append((float(temp[0]), float(temp[1])))
                    elif arg == "y_tick_labels":
                        self.y_tick_labels = [float(x) for x in value.split(",")]
                    else:
                        print("unrecognized argument '{0}' in line {1}, ignore it!".format(arg, row_num+1))
                else:
                    print("Unknown command '{0}' at line {1}".format(line, row_num+1))

                row_num += 1

            # Read data
            size = 0
            while row_num < num_lines:
                line = lines[row_num].strip()
                if len(line) <= 0:
                    row_num += 1
                    continue
                elif line.startswith("#"):
                    row_num += 1
                    continue
                elif line.startswith("supergroup="):
                    arg = line.strip()[len("supergroup="):]
                    if self.supergrouplabels == None:
                        self.supergrouplabels = []

                    if self.blank == None:
                        self.blank = []
                    if (len(self.supergrouplabels)) > 0:
                        self.supergrouplabels[-1][2] = size - 1
                        self.blank.append(size - 1)
                        self.blank.append(size - 1)

                    # -1 as a placeholder
                    self.supergrouplabels.append([arg, size, -1])
                    row_num += 1
                    continue

                elif line.startswith("group="):
                    arg = line.strip()[len("group="):]
                    if self.grouplabels == None:
                        self.grouplabels = []

                    if self.blank == None:
                        self.blank = []

                    if (len(self.grouplabels)) > 0:
                        self.grouplabels[-1][2] = size - 1
                        self.blank.append(size - 1)

                    # -1 as a placeholder
                    self.grouplabels.append([arg, size, -1])
                    row_num += 1
                    continue

                self.read_data_section(line)
                size += 1
                row_num += 1

            # set the ending index of last group
            if (len(self.grouplabels)) > 0:
                self.grouplabels[-1][2] = size - 1

            # set the ending index for the last super group
            if (len(self.supergrouplabels)) > 0:
                self.supergrouplabels[-1][2] = size - 1

            return True

    # This function is main entry which is used to draw the specified kind of graph according to the user specification.
    def draw(self):
        if self.graphKind == GraphKind.SimpleBar or \
                self.graphKind == GraphKind.ClusterBar:
            import clusterBar
            clusterBar.drawClusterBar(self)
        elif self.graphKind == GraphKind.StackedBar or \
                self.graphKind == GraphKind.StackedClusterBar:
            import stackedBar
            stackedBar.drawStackedBar(self)
        elif self.graphKind == GraphKind.MirrorClusterBar:
            import mirrorClusterBar
            mirrorClusterBar.drawClusterBar(self)
        elif self.graphKind == GraphKind.LineChart:
            import lineChart
            lineChart.draw(self)
        else:
            print("Unknown output file format!")
            return
