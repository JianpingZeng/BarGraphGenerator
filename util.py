# Format every item in the data sequence with the specified format string,
# and return that transformed list.
def formatWithSpecifiedFormatStr(data, format_str):
    t_str = format_str % float(data)
    # escape the '%' for compatible with latex
    res_str = ''
    i = 0
    sz = len(t_str)
    while i < sz:
        if i >= 1 and t_str[i] == '%'and t_str[i-1] != '\\':
            res_str += "\\%"
        else:
            res_str += t_str[i]
        i += 1

    return res_str

# This function is used to check whether index range A and B disjoint with each other.
def disjoint(A, B):
    assert A[0] <= A[1] and B[0] <= B[1]
    return A[1] < B[0] or B[1] < A[0]

def getTextDimensions(text, font_size):
    from PIL import ImageFont
    import sys
    if sys.platform == "linux":
        font = ImageFont.truetype("FreeSerif", font_size)
    elif sys.platform == "darwin":
        font = ImageFont.truetype("Microsoft Sans Serif", font_size)
    else:
        font = ImageFont.truetype("Arial", font_size)
    return font.getsize(text)