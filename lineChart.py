import math
import matplotlib.pyplot as plt
import matplotlib as mplib
import matplotlib.ticker as mticker
from GraphObject import GraphObject
from scipy.interpolate import interp1d
from util import formatWithSpecifiedFormatStr

def scale(val, src, dst):
    """
    Scale the given value from the scale of src to the scale of dst.
    """
    if val >= src[0] and val <= src[1]:
        return ((val - src[0]) / (src[1] - src[0])) * (dst[1] - dst[0]) + dst[0]
    else:
        return val

def draw(obj : GraphObject):
    if obj.supergrouplabels != None or obj.grouplabels != None:
        assert("Line chart does not support group labels and super group labels")

    mplib.rcParams['ps.useafm'] = True
    mplib.rcParams['pdf.use14corefonts'] = True
    mplib.rcParams['text.usetex'] = True
    mplib.rcParams.update({'font.size': obj.fontsize})
    fig, ax = plt.subplots(figsize=(obj.fig_width, obj.fig_height))
    ax.set_facecolor('xkcd:white')

    # The following list indicates the sub title for each line.
    x_tick_labels = []
    # The following list saves the height of each kind of bars.
    y_values = []
    for x in obj.column_headers:
        y_values.append([])

    for row in obj.table:
        if not obj.no_xticks_label:
            assert len(row) >= 2, "the number of line in each cluster must be greater than 1!"
            x_tick_labels.append(row[0])
            t = list(map(float, row[1:]))
        else:
            assert len(row) >= 1, "the number of line in each cluster must be greater than 1!"
            t = list(map(float, row[0:]))

        i = 0
        size = len(t)
        while i < size:
            y_values[i].append(t[i])
            i += 1

    # the x axies for each line in a group.
    num_groups = len(y_values[0])
    x_axis = range(num_groups)

    # save the beginning and end x-axies for each grouped bars, which is used to place group label.
    assert len(y_values) == len(obj.column_headers), \
        "the number of bars in each group and number of labels must matches"

    if not obj.mirror_y_axis:
        i = 0
        while i < len(y_values):
            # assert(len(x_axis) == len(y_values[i]))
            scaled_y_values = y_values[i]
            if obj.source_scale != None:
                assert obj.destination_scale != None
                assert len(obj.source_scale) == len(obj.destination_scale)
                j = 0
                while j < len(obj.source_scale):
                    scaled_y_values = [x if x < obj.source_scale[j][0] else
                                       scale(x, obj.source_scale[j], obj.destination_scale[j]) for x in scaled_y_values]
                    j += 1

            if i in obj.hatches:
                ax.plot(x_axis, scaled_y_values, color=obj.colors[i], label=obj.column_headers[i],
                         marker=obj.hatches[i], markersize=obj.markersize, linewidth=obj.linewidth)
            else:
                ax.plot(x_axis, scaled_y_values, color=obj.colors[i], label=obj.column_headers[i],
                         markersize=obj.markersize, linewidth=obj.linewidth)
            i += 1
        # set left y-axis label
        ax.set_ylabel(obj.ylabel, fontsize=obj.y_label_size)

        if obj.source_scale != None:
            # [0, 1, 300, 400]
            assert obj.y_tick_labels != None
            ax.set_yticklabels(obj.y_tick_labels, fontsize=obj.label_size)
        if obj.y_log_scale:
            ax.set_yscale('log', base=10)
    else:
        assert len(y_values) == 2, "Must be only two lines for mirrored y axis!"
        i = 0
        # assert (len(x_axis) == len(y_values[i]))
        if i in obj.hatches:
            ax.plot(x_axis, y_values[i], color=obj.colors[i], label=obj.column_headers[i],
                     marker=obj.hatches[i], markersize=obj.markersize, linewidth=obj.linewidth)
        else:
            ax.plot(x_axis, y_values[i], color=obj.colors[i], label=obj.column_headers[i],
                     markersize=obj.markersize, linewidth=obj.linewidth)

        # set right y-axis label
        ax2 = ax.twinx()
        i = 1
        assert (len(x_axis) == len(y_values[i]))
        if i in obj.hatches:
            ax2.plot(x_axis, y_values[i], color=obj.colors[i], label=obj.column_headers[i],
                     marker=obj.hatches[i], markersize=obj.markersize, linewidth=obj.linewidth)
        else:
            ax2.plot(x_axis, y_values[i], color=obj.colors[i], label=obj.column_headers[i],
                     markersize=obj.markersize, linewidth=obj.linewidth)

        ax2.set_ylabel(obj.right_ylabel, fontsize=obj.y_label_size, color=obj.colors[1])
        if obj.right_ymax != None:
            ax2.set_ylim(top=obj.right_ymax)
        if obj.right_ymin != None:
            ax2.set_ylim(bottom=obj.right_ymin)
        if obj.right_yformat != None:
            ax2.set_yticklabels(ax2.get_yticks(), color=obj.colors[1])

    # Set the orientation of labels in x axis as vertical
    if obj.norotate:
        obj.rotateby = 0

    # set the background grid
    if not obj.no_xticks_label:
        ax.set_xticks(x_axis)
        ax.set_xticklabels(x_tick_labels, rotation=obj.rotateby, fontsize=obj.label_size)

    if obj.ymax != None:
        ax.set_ylim(top=obj.ymax)
    if obj.ymin != None:
        ax.set_ylim(bottom=obj.ymin)
    if obj.yformat != None:
        ax.set_yticklabels(ax.get_yticks(), color=obj.colors[0])

    # set vertical grid lines.
    ax.yaxis.grid(linestyle='--', linewidth=obj.grid_linewidth, dashes=(obj.onink, obj.offink), color='black',
                  which='major')
    # grid lines are behind the rest.
    ax.set_axisbelow(True)

    label_pad = 0
    if obj.grouplabels != None:
        label_pad = 30
    if obj.supergrouplabels != None:
        label_pad += 30

    plt.xlabel(obj.xlabel, labelpad=label_pad, fontsize=obj.x_label_size)

    num_cols = obj.ncol if obj.ncol != None else len(obj.column_headers)
    if obj.over_top_legend:
        legend = plt.legend(loc="upper center", ncol=obj.ncol,
                            fontsize=obj.fontsize, framealpha=0.5, borderaxespad=0)
        plt.draw()  # Draw the figure so you can find the position of the legend.

        # Get the bounding box of the original legend
        bb = legend.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

        # (1,0.41), (2, 0.42), (3, 0.435), (4,0.45), (5,0.46), (6,0.48), (7,0.48)
        xx = [1, 2, 3, 4, 5, 6, 7]
        yy = [1, 0.45, 0.3, 0.2, 0.15, 0.14, 0.1]
        f = interp1d(xx, yy, kind='cubic')

        # Change to location of the legend.
        bb.y1 += (bb.y1 - bb.y0) * f(num_cols) + 0.2
        legend.set_bbox_to_anchor(bb, transform=ax.transAxes)
    elif obj.left_outside_legend:
        legend = plt.legend(loc="upper center",
                            ncol=obj.ncol, fontsize=obj.fontsize, framealpha=0.5, borderaxespad=0)
        plt.draw()  # Draw the figure so you can find the position of the legend.

        # Get the bounding box of the original legend
        bb = legend.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

        # Change to location of the legend.
        # (1,0.41), (2, 0.42), (3, 0.435), (4,0.45), (5,0.46), (6,0.48), (7,0.48)
        xx = [1, 2, 3, 4, 5, 6, 7]
        yy = [0.41, 0.42, 0.435, 0.45, 0.46, 0.48, 0.48]
        f = interp1d(xx, yy, kind='cubic')
        bb.x0 += 0.5 + (bb.x1 - bb.x0) * math.pow(num_cols, f(num_cols))
        legend.set_bbox_to_anchor(bb, transform=ax.transAxes)
    elif obj.horizontal_legend:
        num_cols = obj.ncol if obj.ncol != None else len(obj.column_headers)
        if obj.legend_loc != None:
            legend = plt.legend(ncol=num_cols, loc=obj.legend_loc, fontsize=obj.fontsize)
        else:
            legend = plt.legend(ncol=num_cols, fontsize=obj.fontsize)
    elif obj.legend_loc != None:
        legend = plt.legend(loc=obj.legend_loc, fontsize=obj.fontsize)
    else:
        legend = plt.legend(fontsize=obj.fontsize)

    legend.get_frame().set_alpha(0)
    ax.tick_params(labelleft=True, labelright=True)
    if obj.xnbins != None:
        ax.locator_params(nbins=obj.xnbins, axis='x')
    if obj.ynbins != None:
        ax.locator_params(nbins=obj.ynbins, axis='y')

    if obj.yformat != None:
        ticks_loc = ax.get_yticks().tolist()
        ax.yaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        i = 0
        while i < len(ticks_loc) and ticks_loc[i] <= obj.ymax:
            i += 1
        if i > 0:
            i -= 1
        ticks_loc[i] = formatWithSpecifiedFormatStr(ticks_loc[i], obj.yformat)
        ax.set_yticklabels(ticks_loc)

    plt.savefig(obj.output, dpi=400, bbox_inches='tight', pad_inches=0)
    plt.close()