import math
import operator

import matplotlib.pyplot as plt
import matplotlib as mplib
from scipy.interpolate import interp1d
import matplotlib.ticker as mticker

from util import formatWithSpecifiedFormatStr, disjoint

def drawStackedBar(obj):
    # Set the figure size
    mplib.rcParams['ps.useafm'] = True
    mplib.rcParams['pdf.use14corefonts'] = True
    mplib.rcParams['text.usetex'] = True
    mplib.rcParams.update({'font.size': obj.fontsize})
    fig = plt.figure(figsize=(obj.fig_width, obj.fig_height))
    ax = plt.gca()
    #plt.style.use('ggplot')
    ax.set_facecolor('xkcd:white')

    # The following list indicates the sub title for each bar.
    x_tick_labels = []
    table = {}
    for row in obj.table:
        assert len(row) >= 2, "the number of each row of cluster bar must greater than 1!"
        x_tick_labels.append(row[0])
        t = list(map(float, row[1:]))
        j = 0
        size = len(t)
        # The number of columns equals number of column headers.
        assert size == len(obj.column_headers)
        while j < size:
            # Group the stacked data by row name, such as WCDL, or application name.
            col_name = obj.column_headers[j]
            if not col_name in table.keys():
                # The following list saves the height of each kind of bars.
                table[col_name] = []

            table[col_name].append(t[j])
            j += 1

    # Compute ticks of x-axis
    num_rows = len(obj.table)
    bar_width = 1
    blank_width = bar_width * obj.group_margin
    x_axis = [1 + x * bar_width for x in range(0, num_rows)]

    # shift right those elements after the blank index.
    if obj.blank != None:
        for margin_idx in obj.blank:
            i = margin_idx + 1
            while i < num_rows:
                x_axis[i] += blank_width
                i += 1

    # Draw stacked bars
    all_bars = []
    j = 0
    # the bottom bar has no bottom
    # When the following for loop finishes, bottom_value array contains a series of number
    # indicating the height of stacked bars in each column.
    # This array will be used for computing the place where we should place value label on exceeding high bar.
    bottom_value = [0] * num_rows
    for key, y_values in table.items():
        # key means the x axis label of this stacked bar.
        if j in obj.hatches:
            all_bars += plt.bar(x_axis, y_values, bar_width,
                                edgecolor='black',
                                color=obj.colors[j],
                                label=obj.column_headers[j],
                                bottom=bottom_value,
                                hatch=obj.hatches[j])
        else:
            all_bars += plt.bar(x_axis, y_values, bar_width,
                                edgecolor='black',
                                color=obj.colors[j],
                                label=obj.column_headers[j],
                                bottom=bottom_value)

        bottom_value = list(map(operator.add, bottom_value, y_values))
        j += 1

    # Set the orientation of labels in x axis as vertical
    if obj.norotate:
        obj.rotateby = 0

    # set the background grid
    ax.set_xticks(x_axis)
    if not obj.no_xticks_label:
        ax.set_xticklabels(x_tick_labels, rotation=obj.rotateby, fontsize=obj.x_label_size,
                           horizontalalignment='center', verticalalignment='top')
    else:
        ax.set_xticklabels([])

    if obj.ymax != None:
        plt.ylim(top=obj.ymax)
    if obj.ymin != None:
        plt.ylim(bottom=obj.ymin)

    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()
    y_range = ymax - ymin
    x_range = xmax - xmin
    yticks = ax.get_yticks()

    # difference between two y axis major tick
    y_tick_diff = yticks[1] - yticks[0]

    # this y_scale_portion is automatically adjust the position of group label and super group label.
    y_scale_portion = float(y_tick_diff) / y_range * 0.5

    bbox = fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    fig_width, fig_height = bbox.width * fig.dpi, bbox.height * fig.dpi

    # compute the largest x tick label.
    greatest_x_tick_labels = 0
    if not obj.no_xticks_label:
        for l in ax.get_xticklabels():
            box = l.get_window_extent(renderer=fig.canvas.get_renderer())
            height = box.y1 - box.y0
            height = height / fig_height * y_range
            if height > greatest_x_tick_labels:
                greatest_x_tick_labels = height

    plt.ylabel(obj.ylabel, fontsize=obj.y_label_size)
    if obj.yformat != None:
        ticks_loc = ax.get_yticks().tolist()
        ax.yaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax.set_yticklabels([formatWithSpecifiedFormatStr(x, obj.yformat) for x in ticks_loc])

    yticks = ax.get_yticks()
    xticks = ax.get_xticks()

    # difference between two y axis major tick
    y_tick_diff = yticks[1] - yticks[0]
    x_tick_diff = xticks[1] - xticks[0]
    # this y_scale_portion is automatically adjust the position of group label and super group label.
    y_scale_portion = float(y_tick_diff) / y_range * 0.5
    x_scale_portion = float(x_tick_diff) / x_range
    group_label_height = 0
    if obj.no_xticks_label:
        group_label_Y = ymin
    else:
        group_label_Y = ymin - greatest_x_tick_labels - 0.2 * y_tick_diff
    group_offset = group_label_Y
    # set x axie group label for a group of bars.
    if obj.grouplabels != None:
        if obj.no_xticks_label:
            group_offset = group_label_Y
        else:
            group_offset = group_label_Y - y_scale_portion * y_range * 0.8
        if obj.y_log_scale:
            if group_offset < 0:
                pos = -math.log10(-group_offset)
            else:
                pos = math.log10(group_offset)
            group_offset = math.pow(10, pos*1.1)

        # automatically adjust x axis if two adjacent bar are too close.
        prev_x = None
        i = 0
        for item in obj.grouplabels:
            i += 1
            # Place group label on the given x ticks which are computed by the following equation.
            label, idx1, idx2 = item[0], item[1], item[2]
            tick1 = x_axis[idx1]
            tick2 = x_axis[idx2]
            x = tick1 + (tick2-tick1) * math.cos(obj.group_rotateby/360) * x_scale_portion

            # automatically adjust x axis if two adjacent bar are too close.
            if prev_x != None:
                if x - prev_x < 1.1 * (tick2 - tick1)*x_scale_portion:
                    x += 5*x_scale_portion

            prev_x = x

            y = group_offset
            # call plt.text function to place group label
            text_rantangel = plt.text(x, y, label, fontsize=obj.grouplabel_font, rotation=obj.group_rotateby,
                                      horizontalalignment='left', verticalalignment='top')
            bbox = text_rantangel.get_window_extent(renderer=fig.canvas.get_renderer())
            height = bbox.y1 - bbox.y0
            height = height / fig_height * y_range * 1.25
            if height > group_label_height:
                group_label_height = height

    # increase the group_label_height by 20% for small font.
    group_label_height = group_offset - group_label_height* (1.2 if obj.group_rotateby != 0 else 2)
    # Draw super group labels.
    if obj.supergrouplabels != None:
        # if obj.grouplabels != None:
        #     # adjust more space if it has group label
        #     super_group_offset = -y_scale_portion * y_range + group_label_height
        # else:
        super_group_offset = group_label_height
        if obj.y_log_scale:
            if super_group_offset < 0:
                pos = math.log10(ymin) - math.log10(-super_group_offset)
            else:
                pos = math.log10(ymin) + math.log10(super_group_offset)
            super_group_offset = math.pow(10, pos*1.1)

        for item in obj.supergrouplabels:
            if not (0 <= item[1] and item[1] <= item[2] and item[2] < num_rows):
                print("illegal index range in super group labels, index should in legal range!")
                exit(0)

        for item in obj.supergrouplabels:
            label, idx1, idx2 = item[0], item[1], item[2]
            x1 = x_axis[idx1]
            x2 = x_axis[idx2]
            x = (x1 + x2) / 2.0
            y = super_group_offset
            # call plt.text function to place group label
            plt.text(x, y, label, fontsize=obj.supergrouplabel_font,
                     horizontalalignment='center', verticalalignment='bottom')

    # This variable is used for automatically adjusting the y coordinate of value label
    # of such exceeding bar in order to avoid overlapping.
    texts = []
    if obj.value_label_top_bar:
        i = 0
        prev_y = ymax
        prev_x = -1
        rects = ax.patches
        rects = sorted(rects, key=lambda x: x._x0)
        for rect in rects:
            height = rect.get_height()
            if height > ymax:
                if prev_x != -1 and (i - prev_x) <= 2:
                    y = prev_y + 0.4*y_tick_diff
                else:
                    y = ymax + 0.1*y_tick_diff

                prev_x = i
                prev_y = y
                truncatedVal = "%.1f" % (height)
                x = rect.get_x()
                if obj.yformat != None:
                    truncatedVal = formatWithSpecifiedFormatStr(truncatedVal, obj.yformat)[0]
                texts.append(ax.text(x, y, truncatedVal, ha='center', va='bottom', fontsize=obj.x_label_size))
            i += 1

    # set vertical grid lines.
    ax.yaxis.grid(linestyle='--', linewidth=obj.grid_linewidth, dashes=(obj.onink, obj.offink), color='black', which='major')
    # grid lines are behind the rest.
    ax.set_axisbelow(True)

    num_cols = obj.ncol if obj.ncol != None else len(obj.column_headers)
    if obj.over_top_legend:
        legend = plt.legend(loc="upper center", ncol=obj.ncol,
                            fontsize=obj.fontsize, framealpha=0.5, borderaxespad=0)
        plt.draw()  # Draw the figure so you can find the position of the legend.

        # Get the bounding box of the original legend
        bb = legend.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

        # (1,0.41), (2, 0.42), (3, 0.435), (4,0.45), (5,0.46), (6,0.48), (7,0.48)
        xx = [1, 2, 3, 4, 5, 6, 7]
        yy = [1, 0.45, 0.3, 0.2, 0.15, 0.14, 0.1]
        f = interp1d(xx, yy, kind='cubic')

        # Change to location of the legend.
        bb.y1 += 0.2 + (bb.y1 - bb.y0) * f(num_cols)
        legend.set_bbox_to_anchor(bb, transform=ax.transAxes)
    elif obj.left_outside_legend:
        legend = plt.legend(loc="upper center",
                            ncol=obj.ncol, fontsize=obj.fontsize, framealpha=0.5, borderaxespad=0)
        plt.draw()  # Draw the figure so you can find the position of the legend.

        # Get the bounding box of the original legend
        bb = legend.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

        # Change to location of the legend.
        # (1,0.41), (2, 0.42), (3, 0.435), (4,0.45), (5,0.46), (6,0.48), (7,0.48)
        xx = [1, 2, 3, 4, 5, 6, 7]
        yy = [0.41, 0.42, 0.435, 0.45, 0.46, 0.48, 0.48]
        f = interp1d(xx, yy, kind='cubic')
        bb.x0 += 0.5 + (bb.x1 - bb.x0) * math.pow(num_cols, f(num_cols))
        legend.set_bbox_to_anchor(bb, transform=ax.transAxes)
    elif obj.horizontal_legend:
        if obj.legend_loc != None:
            legend = plt.legend(ncol=num_cols, loc=obj.legend_loc, fontsize=obj.fontsize)
        else:
            legend = plt.legend(ncol=num_cols, fontsize=obj.fontsize)
    elif obj.legend_loc != None:
        legend = plt.legend(loc=obj.legend_loc, fontsize=obj.fontsize)
    else:
        legend = plt.legend(fontsize=obj.fontsize)

    legend.get_frame().set_alpha(0)
    plt.tick_params(labelleft=True, labelright=True)
    if obj.xnbins != None:
        ax.locator_params(nbins=obj.xnbins, axis='x')
    if obj.ynbins != None:
        ax.locator_params(nbins=obj.ynbins, axis='y')
    if obj.y_log_scale:
        ax.set_yscale('log', base=10)

    plt.savefig(obj.output, dpi=400, bbox_inches = 'tight', pad_inches = 0)
    plt.close()