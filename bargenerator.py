#!/usr/bin/python3
# author Jianping Zeng (jpzeng@vt.edu)
# This is a bar graph generator developed by Jianping Zeng, which is inspired by bargraph developed by Derek Bruening.
# Base on this, this tool accepts same input file format as the bargraph tool.
import argparse
import os
from GraphObject import GraphObject

if __name__ == "__main__":
    # Add some command line options, such as input original data file, an option
    # used to specify what kind of figure format to be generated (jpeg, png, eps, pdf).
    parser = argparse.ArgumentParser(description="Bar graph generator", prog="bargenerator")
    legal_output_formats = ["svg", "png", "pdf"]

    parser.add_argument("input",
                        metavar="<input file>", type=str,
                        help='specify the input file')
    parser.add_argument("-l", "--language", metavar="language",
                        choices=legal_output_formats,
                        default="pdf",
                        help="choose output figure kind (default by pdf)")
    parser.add_argument("-o", "--output", metavar="output file",
                        help="specify the path to the output file")
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print("the input file doesn't exists")
        exit(1)
    if not args.output.split('.')[-1] in legal_output_formats:
        print("the output file format isn't allowed, must be svg, png, pdf")
        exit(1)

    graph_object = GraphObject(args.output, args.language)

    if (not graph_object.parse_input_file(args.input)):
        print("Failed to parse the input file")
        exit(1)

    # Call the drawing function to draw the specified kind of graph.
    graph_object.draw()

