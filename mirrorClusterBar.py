import math

from GraphObject import GraphObject
import matplotlib.pyplot as plt
import matplotlib as mplib

from util import formatWithSpecifiedFormatStr, disjoint


def drawClusterBar(obj : GraphObject):
    # plt.style.use('ggplot')
    # Set the figure size
    mplib.rcParams['ps.useafm'] = True
    mplib.rcParams['pdf.use14corefonts'] = True
    mplib.rcParams['text.usetex'] = True
    mplib.rcParams.update({'font.size' : obj.fontsize})
    fig, ax = plt.subplots(figsize=(obj.fig_width, obj.fig_height))
    ax.set_facecolor('xkcd:white')

    # The following list indicates the sub title for each bar.
    x_tick_labels = []
    # The following list saves the height of each kind of bars.
    y_values = []
    for x in obj.column_headers:
        y_values.append([])

    for row in obj.table:
        assert len(row) == 3, "the number of each row of cluster bar must be 3 for mirrored cluster bar graph!"
        x_tick_labels.append(row[0])
        t = list(map(float, row[1:]))

        i = 0
        size = len(t)
        while i < size:
            y_values[i].append(t[i])
            i += 1

    bar_width = 1
    margin_group_bars = 1.5 * bar_width
    blank_width = bar_width * obj.group_margin

    diff = math.ceil((len(obj.column_headers) - 1) * bar_width + margin_group_bars)

    # the x axies for each bar in a group.
    num_groups = len(y_values[0])
    x_axis = [1 + x * diff for x in range(0, num_groups)]

    # shift right those elements after the blank index.
    if obj.blank != None:
        for margin_idx in obj.blank:
            i = margin_idx + 1
            while i < num_groups:
                x_axis[i] += blank_width
                i += 1

    # save the beginning and end x-axies for each grouped bars, which is used to place group label.
    assert len(y_values) == len(obj.column_headers), \
        "the number of bars in each group and number of labels must matches"

    all_bars = []
    # first plot bars indexed by even number
    i = 0
    group_bars = y_values[i]
    if i in obj.hatches:
        all_bars += ax.bar(x_axis, group_bars, bar_width, edgecolor='black',
                            color=obj.colors[i],
                            hatch=obj.hatches[i])
    else:
        all_bars += ax.bar(x_axis, group_bars, bar_width, edgecolor='black',
                            color=obj.colors[i])

    x_axis = [x + bar_width for x in x_axis]

    # set left y-axis label
    ax.set_ylabel(obj.ylabel, fontsize=obj.y_label_size, color=obj.colors[0])
    if obj.ymax != None:
        ax.set_ylim(top=obj.ymax)
    if obj.ymin != None:
        ax.set_ylim(bottom=obj.ymin)
    if obj.yformat != None:
        ax.set_yticklabels(ax.get_yticks(), color=obj.colors[0])

    # set right y-axis label
    ax2 = ax.twinx()
    group_bars = y_values[1]
    if 1 in obj.hatches:
        all_bars += ax2.bar(x_axis, group_bars, bar_width, edgecolor='black',
                            color=obj.colors[1],
                            hatch=obj.hatches[1])
    else:
        all_bars += ax2.bar(x_axis, group_bars, bar_width, edgecolor='black',
                            color=obj.colors[1])
    x_axis = [x + bar_width for x in x_axis]

    ax2.set_ylabel(obj.right_ylabel, fontsize=obj.y_label_size, color=obj.colors[1])
    if obj.right_ymax != None:
        ax2.set_ylim(top=obj.right_ymax)
    if obj.right_ymin != None:
        ax2.set_ylim(bottom=obj.right_ymin)
    if obj.right_yformat != None:
        ax2.set_yticklabels(ax2.get_yticks(), color=obj.colors[1])


    # compute x axis label for each group of bars
    x_ticks = [x - diff/2.0 for x in x_axis]
    # set the background grid
    ax.set_xticks(x_ticks)

    # Set the orientation of labels in x axis as vertical

    if obj.norotate:
        obj.rotateby = 0

    ax.set_xticklabels(x_tick_labels, rotation=obj.rotateby, fontsize=obj.label_size)

    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()
    y_range = ymax - ymin
    x_range = xmax - xmin

    yticks = ax.get_yticks()
    xticks = ax.get_xticks()

    # difference between two y axis major tick
    y_tick_diff = min(yticks[1] - yticks[0], ax2.get_yticks()[1] - ax2.get_yticks()[0])
    x_tick_diff = xticks[1] - xticks[0]

    # this y_scale_portion is automatically adjust the position of group label and super group label.
    y_scale_portion = float(y_tick_diff)/y_range * 0.5
    x_scale_portion = float(x_tick_diff)/x_range

    bbox = fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    fig_width, fig_height = bbox.width * fig.dpi, bbox.height * fig.dpi

    # compute the largest x tick label.
    greatest_x_tick_labels = 0
    for l in ax.get_xticklabels():
        box = l.get_window_extent(renderer=fig.canvas.get_renderer())
        height = box.y1 - box.y0
        height = height/fig_height * min(y_range, obj.right_ymax - obj.right_ymin)
        if height > greatest_x_tick_labels:
            greatest_x_tick_labels = height

    group_label_Y = ymin - greatest_x_tick_labels - 0.2*y_tick_diff
    group_label_height = 0
    obj.group_offset = group_label_Y - y_scale_portion*y_range * 0.8
    # set x axie group label for a group of bars.
    if obj.grouplabels != None and len(obj.grouplabels) > 0:
        # Check if the bar index range is legal, such as the index isn't less than 0 and greater than
        # the number of bars.
        # Also, each pair of indices should disjoint with each other.
        for item in obj.grouplabels:
            if not (0 <= item[1] and item[1] <= item[2] and item[2] <= num_groups):
                print("illegal index range in group labels, index should in legal range!")
                exit(0)
        i = 0
        size = len(obj.grouplabels)
        while i < size:
            j = i + 1
            while j < size:
                if not disjoint([obj.grouplabels[i][1], obj.grouplabels[i][2]],
                                [obj.grouplabels[j][1], obj.grouplabels[j][2]]):
                    print("illegal index range in group labels, index range should disjoint with each other!")
                    exit(0)
                j += 1
            i += 1

        # automatically adjust x axis if two adjacent bar are too close.
        prev_x = None
        i = 0
        for item in obj.grouplabels:
            i += 1
            # Place group label on the given x ticks which are computed by the following equation.
            label, idx1, idx2 = item[0], item[1], item[2]
            tick1 = x_axis[idx1] - diff #+ margin_group_bars
            tick2 = x_axis[idx2]
            x = tick1 + (tick2 - tick1) * math.cos(obj.group_rotateby / 360) * x_scale_portion

            # automatically adjust x axis if two adjacent bar are too close.
            if prev_x != None:
                if x - prev_x < 1.1 * (tick2 - tick1)*x_scale_portion:
                    x += 5*x_scale_portion

            prev_x = x

            y = obj.group_offset
            # call plt.text function to place group label
            text_rantangel = plt.text(x, y, label, fontsize=obj.grouplabel_font, rotation=obj.group_rotateby,
                                      horizontalalignment='left', verticalalignment='top')
            bbox = text_rantangel.get_window_extent(renderer=fig.canvas.get_renderer())
            height = bbox.y1 - bbox.y0
            height = height / fig_height * y_range * 1.25
            if height > group_label_height:
                group_label_height = height

    # increase the group_label_height by 20% for small font.
    # Draw super group labels.
    group_label_height = obj.group_offset - group_label_height * (1.2 if obj.group_rotateby != 0 else 2)
    if obj.supergrouplabels != None and len(obj.supergrouplabels) > 0:
        # adjust more space if it has group label
        obj.super_group_offset = group_label_height - y_scale_portion * y_range * 0.8 * obj.fig_height / 8

        for item in obj.supergrouplabels:
            if not (0 <= item[1] and item[1] <= item[2] and item[2] <= num_groups):
                print("illegal index range in super group labels, index should in legal range!")
                exit(0)
        i = 0
        size = len(obj.supergrouplabels)
        while i < size:
            j = i + 1
            while j < size:
                if not disjoint([obj.supergrouplabels[i][1], obj.supergrouplabels[i][2]],
                                [obj.supergrouplabels[j][1], obj.supergrouplabels[j][2]]):
                    print("illegal index range in super group labels, index range should disjoint with each other!")
                    exit(0)
                j += 1
            i += 1

        for item in obj.supergrouplabels:
            label, idx1, idx2 = item[0], item[1], item[2]
            tick1 = x_axis[idx1] - diff + margin_group_bars
            tick2 = x_axis[idx2]
            x = (tick1 + tick2)/2.0
            y = obj.super_group_offset
            # call plt.text function to place group label
            plt.text(x, y, label, fontsize=obj.supergrouplabel_font, horizontalalignment='center', verticalalignment='bottom')

    # set vertical grid lines.
    ax.yaxis.grid(linestyle='--', linewidth=obj.grid_linewidth, dashes=(obj.onink, obj.offink), color='black', which='major')
    # grid lines are behind the rest.
    ax.set_axisbelow(True)

    label_pad = 0
    if obj.grouplabels != None:
        label_pad = 30
    if obj.supergrouplabels != None:
        label_pad += 30

    plt.xlabel(obj.xlabel, labelpad=label_pad, fontsize=obj.x_label_size)

    # place annotation text on the locations specified by obj.extraops
    if obj.extraops != None and obj.ymax != None:
        for extraops_item in obj.extraops:
            x = extraops_item.label_loc[0]
            y = extraops_item.label_loc[1]
            assert x != None and y != None, \
                "must specify x and y coordinate for the annotation text in cluster graph"
            assert x >= 0 and x < num_groups and y >= 0 and y < len(obj.column_headers), \
                "illegal [x, y] coordinates for the annotation text"
            bar_idx = y * num_groups + x
            bar = all_bars[bar_idx]
            x_coor, y_coor = bar.get_x() + bar_width / 2.0, ax.get_ylim()[1]*1.005
            ax.text(x_coor, y_coor, extraops_item.label, fontname=extraops_item.font_name, fontsize=extraops_item.font_size)

    # place value label for each bar on the top of bar

    # This variable is used for automatically adjusting the y coordinate of value label
    # of such exceeding bar in order to avoid overlapping.
    texts = []
    if obj.value_label_top_bar:
        i = 0
        prev_y = ymax
        prev_x = -1
        rects = ax.patches
        rects = sorted(rects, key=lambda x: x._x0)
        for rect in rects:
            height = rect.get_height()
            if height > ymax:
                if prev_x != -1 and (i - prev_x) <= 2:
                    y = prev_y + 0.4*y_tick_diff
                else:
                    y = ymax + 0.1*y_tick_diff

                prev_x = i
                prev_y = y
                truncatedVal = "%.1f" % (height)
                x = rect.get_x()
                if obj.yformat != None:
                    truncatedVal = formatWithSpecifiedFormatStr(truncatedVal, obj.yformat)[0]
                texts.append(ax.text(x, y, truncatedVal, ha='center', va='bottom', fontsize=obj.x_label_size, color=obj.colors[0]))
            i += 1


        i = 0
        ymax = obj.right_ymax
        prev_y = obj.right_ymax
        prev_x = -1
        rects = ax2.patches
        rects = sorted(rects, key=lambda x: x._x0)
        for rect in rects:
            height = rect.get_height()
            if height > ymax:
                if prev_x != -1 and (i - prev_x) <= 2:
                    y = prev_y + 0.4*y_tick_diff
                else:
                    y = ymax + 0.1*y_tick_diff

                prev_x = i
                prev_y = y
                truncatedVal = "%.1f" % (height)
                x = rect.get_x()
                if obj.right_yformat != None:
                    truncatedVal = formatWithSpecifiedFormatStr(truncatedVal, obj.right_yformat)[0]
                texts.append(ax2.text(x, y, truncatedVal, ha='center', va='bottom', fontsize=obj.x_label_size, color=obj.colors[1]))
            i += 1

    if obj.over_top_legend:
        legend = plt.legend(loc=obj.legend_loc, bbox_to_anchor=(0, 1.05), ncol=obj.ncol, fontsize=obj.fontsize)
    elif obj.left_outside_legend:
        legend = plt.legend(loc=obj.legend_loc, bbox_to_anchor=(1.05, 1), ncol=obj.ncol, fontsize=obj.fontsize)
    elif obj.horizontal_legend:
        num_cols = obj.ncol if obj.ncol != None else len(obj.column_headers)
        if obj.legend_loc != None:
            legend = plt.legend(ncol=num_cols, loc=obj.legend_loc, fontsize=obj.fontsize)
        else:
            legend = plt.legend(ncol=num_cols, fontsize=obj.fontsize)
    elif obj.legend_loc != None:
        legend = plt.legend(loc=obj.legend_loc, fontsize=obj.fontsize)
    else:
        legend = plt.legend(fontsize=obj.fontsize)

    legend.get_frame().set_alpha(0)
    if obj.xnbins != None:
        ax.locator_params(nbins=obj.xnbins, axis='x')
    if obj.ynbins != None:
        ax.locator_params(nbins=obj.ynbins, axis='y')
    plt.savefig(obj.output, dpi=400, bbox_inches = 'tight', pad_inches = 0)
    plt.close()
