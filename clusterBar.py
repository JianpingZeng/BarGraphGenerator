import math

from GraphObject import GraphObject
import matplotlib.pyplot as plt
import matplotlib as mplib
from scipy.interpolate import interp1d
import matplotlib.ticker as mticker

from util import formatWithSpecifiedFormatStr, disjoint


def drawClusterBar(obj : GraphObject):
    # plt.style.use('ggplot')
    # Set the figure size
    mplib.rcParams['ps.useafm'] = True
    mplib.rcParams['pdf.use14corefonts'] = True
    mplib.rcParams['text.usetex'] = True
    mplib.rcParams.update({'font.size' : obj.fontsize})
    fig = plt.figure(figsize=(obj.fig_width, obj.fig_height))
    ax = plt.gca()
    ax.set_facecolor('xkcd:white')

    # The following list indicates the sub title for each bar.
    x_tick_labels = []
    # The following list saves the height of each kind of bars.
    y_values = []
    for x in obj.column_headers:
        y_values.append([])

    for row in obj.table:
        assert len(row) >= 2, "the number of bar in each cluster must be greater than 1!"
        x_tick_labels.append(row[0])
        t = list(map(float, row[1:]))

        i = 0
        size = len(t)
        while i < size:
            y_values[i].append(t[i])
            i += 1

    bar_width = 1
    margin_group_bars = 1.5 * bar_width
    blank_width = bar_width * obj.group_margin

    diff = math.ceil((len(obj.column_headers) - 1) * bar_width + margin_group_bars)

    # the x axies for each bar in a group.
    num_groups = len(y_values[0])
    x_axis = [1 + x * diff for x in range(0, num_groups)]

    # shift right those elements after the blank index.
    if obj.blank != None:
        for margin_idx in obj.blank:
            i = margin_idx + 1
            while i < num_groups:
                x_axis[i] += blank_width
                i += 1

    # save the beginning and end x-axies for each grouped bars, which is used to place group label.
    assert len(y_values) == len(obj.column_headers), \
        "the number of bars in each group and number of labels must matches"

    all_bars = []
    i = 0
    while i < len(y_values):
        group_bars = y_values[i]
        if i in obj.hatches:
            all_bars += plt.bar(x_axis, group_bars, bar_width, edgecolor='black',
                                color=obj.colors[i], label=obj.column_headers[i],
                                hatch=obj.hatches[i])
        else:
            all_bars += plt.bar(x_axis, group_bars, bar_width, edgecolor='black',
                                color=obj.colors[i], label=obj.column_headers[i])

        x_axis = [x + bar_width for x in x_axis]
        i += 1

    # compute x axis label for each group of bars
    x_ticks = [x - diff/2.0 for x in x_axis]
    # set the background grid
    ax.set_xticks(x_ticks)

    # Set the orientation of labels in x axis as vertical

    if obj.norotate:
        obj.rotateby = 0

    ax.set_xticklabels(x_tick_labels, rotation=obj.rotateby, fontsize=obj.label_size)

    if obj.ymax != None:
        plt.ylim(top=obj.ymax)
    if obj.ymin != None:
        plt.ylim(bottom=obj.ymin)

    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()
    y_range = ymax - ymin
    x_range = xmax - xmin

    yticks = ax.get_yticks()
    xticks = ax.get_xticks()

    # difference between two y axis major tick
    y_tick_diff = yticks[1] - yticks[0]
    x_tick_diff = xticks[1] - xticks[0]

    # this y_scale_portion is automatically adjust the position of group label and super group label.
    y_scale_portion = float(y_tick_diff)/y_range * 0.5
    x_scale_portion = float(x_tick_diff)/x_range

    bbox = fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    fig_width, fig_height = bbox.width * fig.dpi, bbox.height * fig.dpi

    # compute the largest x tick label.
    greatest_x_tick_labels = 0
    for l in ax.get_xticklabels():
        box = l.get_window_extent(renderer=fig.canvas.get_renderer())
        height = box.y1 - box.y0
        height = height/fig_height * y_range
        if height > greatest_x_tick_labels:
            greatest_x_tick_labels = height

    plt.ylabel(obj.ylabel, fontsize=obj.y_label_size)

    group_label_Y = ymin - greatest_x_tick_labels - 0.2*y_tick_diff
    group_label_height = 0
    # set x axie group label for a group of bars.
    if obj.grouplabels != None:
        obj.group_offset = group_label_Y - y_scale_portion*y_range * 0.8

        # Check if the bar index range is legal, such as the index isn't less than 0 and greater than
        # the number of bars.
        # Also, each pair of indices should disjoint with each other.
        for item in obj.grouplabels:
            if not (0 <= item[1] and item[1] <= item[2] and item[2] <= num_groups):
                print("illegal index range in group labels, index should in legal range!")
                exit(0)
        i = 0
        size = len(obj.grouplabels)
        while i < size:
            j = i + 1
            while j < size:
                if not disjoint([obj.grouplabels[i][1], obj.grouplabels[i][2]],
                                [obj.grouplabels[j][1], obj.grouplabels[j][2]]):
                    print("illegal index range in group labels, index range should disjoint with each other!")
                    exit(0)
                j += 1
            i += 1

        # automatically adjust x axis if two adjacent bar are too close.
        prev_x = None
        i = 0
        for item in obj.grouplabels:
            i += 1
            # Place group label on the given x ticks which are computed by the following equation.
            label, idx1, idx2 = item[0], item[1], item[2]
            tick1 = x_axis[idx1] - diff #+ margin_group_bars
            tick2 = x_axis[idx2]
            x = tick1 + (tick2 - tick1) * math.cos(obj.group_rotateby / 360) * x_scale_portion

            # automatically adjust x axis if two adjacent bar are too close.
            if prev_x != None:
                if x - prev_x < 1.1 * (tick2 - tick1)*x_scale_portion:
                    x += 5*x_scale_portion

            prev_x = x

            y = obj.group_offset
            # call plt.text function to place group label
            text_rantangel = plt.text(x, y, label, fontsize=obj.grouplabel_font, rotation=obj.group_rotateby,
                                      horizontalalignment='left', verticalalignment='top')
            bbox = text_rantangel.get_window_extent(renderer=fig.canvas.get_renderer())
            height = bbox.y1 - bbox.y0
            height = height / fig_height * y_range * 1.25
            if height > group_label_height:
                group_label_height = height

    # increase the group_label_height by 20% for small font.
    # Draw super group labels.
    group_label_height = obj.group_offset - group_label_height * (1.2 if obj.group_rotateby != 0 else 2)
    if obj.supergrouplabels != None and len(obj.supergrouplabels) > 0:
        # adjust more space if it has group label
        obj.super_group_offset = group_label_height - y_scale_portion * y_range * 0.4 * obj.fig_height / 16

        for item in obj.supergrouplabels:
            if not (0 <= item[1] and item[1] <= item[2] and item[2] <= num_groups):
                print("illegal index range in super group labels, index should in legal range!")
                exit(0)
        i = 0
        size = len(obj.supergrouplabels)
        while i < size:
            j = i + 1
            while j < size:
                if not disjoint([obj.supergrouplabels[i][1], obj.supergrouplabels[i][2]],
                                [obj.supergrouplabels[j][1], obj.supergrouplabels[j][2]]):
                    print("illegal index range in super group labels, index range should disjoint with each other!")
                    exit(0)
                j += 1
            i += 1

        for item in obj.supergrouplabels:
            label, idx1, idx2 = item[0], item[1], item[2]
            tick1 = x_axis[idx1] - diff + margin_group_bars
            tick2 = x_axis[idx2]
            x = (tick1 + tick2)/2.0
            y = obj.super_group_offset
            # call plt.text function to place group label
            plt.text(x, y, label, fontsize=obj.supergrouplabel_font, horizontalalignment='center', verticalalignment='bottom')

    # set vertical grid lines.
    ax.yaxis.grid(linestyle='--', linewidth=obj.grid_linewidth, dashes=(obj.onink, obj.offink), color='black', which='major')
    # grid lines are behind the rest.
    ax.set_axisbelow(True)

    label_pad = 0
    if obj.grouplabels != None:
        label_pad = 30
    if obj.supergrouplabels != None:
        label_pad += 30

    plt.xlabel(obj.xlabel, labelpad=label_pad, fontsize=obj.x_label_size)

    # place annotation text on the locations specified by obj.extraops
    if obj.extraops != None and obj.ymax != None:
        for extraops_item in obj.extraops:
            x = extraops_item.label_loc[0]
            y = extraops_item.label_loc[1]
            assert x != None and y != None, \
                "must specify x and y coordinate for the annotation text in cluster graph"
            assert x >= 0 and x < num_groups and y >= 0 and y < len(obj.column_headers), \
                "illegal [x, y] coordinates for the annotation text"
            bar_idx = y * num_groups + x
            bar = all_bars[bar_idx]
            x_coor, y_coor = bar.get_x() + bar_width / 2.0, ax.get_ylim()[1]*1.005
            ax.text(x_coor, y_coor, extraops_item.label, fontname=extraops_item.font_name, fontsize=extraops_item.font_size)

    # place value label for each bar on the top of bar

    # This variable is used for automatically adjusting the y coordinate of value label
    # of such exceeding bar in order to avoid overlapping.
    texts = []
    if obj.value_label_top_bar:
        i = 0
        prev_y = ymax
        prev_x = -1
        rects = ax.patches
        rects = sorted(rects, key=lambda x: x._x0)
        for rect in rects:
            height = rect.get_height()
            if height > ymax:
                if prev_x != -1 and (i - prev_x) <= 2:
                    y = prev_y + 0.4*y_tick_diff
                else:
                    y = ymax + 0.1*y_tick_diff

                prev_x = i
                prev_y = y
                truncatedVal = "%.1f" % (height)
                x = rect.get_x()
                if obj.yformat != None:
                    truncatedVal = formatWithSpecifiedFormatStr(truncatedVal, obj.yformat)
                texts.append(ax.text(x, y, truncatedVal, ha='center', va='bottom', fontsize=obj.x_label_size))
            i += 1

    num_cols = obj.ncol if obj.ncol != None else len(obj.column_headers)
    if obj.over_top_legend:
        legend = plt.legend(loc="upper center", ncol=obj.ncol,
                            fontsize=obj.fontsize, framealpha=0.5, borderaxespad=0)
        plt.draw()  # Draw the figure so you can find the position of the legend.

        # Get the bounding box of the original legend
        bb = legend.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

        # (1,0.41), (2, 0.42), (3, 0.435), (4,0.45), (5,0.46), (6,0.48), (7,0.48)
        xx = [1, 2, 3, 4, 5, 6, 7]
        yy = [1, 0.45, 0.3, 0.2, 0.15, 0.14, 0.1]
        f = interp1d(xx, yy, kind='cubic')

        # Change to location of the legend.
        bb.y1 += (bb.y1 - bb.y0) * f(num_cols)
        legend.set_bbox_to_anchor(bb, transform=ax.transAxes)
    elif obj.left_outside_legend:
        legend = plt.legend(loc="upper center",
                            ncol=obj.ncol, fontsize=obj.fontsize, framealpha=0.5, borderaxespad=0)
        plt.draw()  # Draw the figure so you can find the position of the legend.

        # Get the bounding box of the original legend
        bb = legend.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

        # Change to location of the legend.
        # (1,0.41), (2, 0.42), (3, 0.435), (4,0.45), (5,0.46), (6,0.48), (7,0.48)
        xx = [1, 2, 3, 4, 5, 6, 7]
        yy = [0.41, 0.42, 0.435, 0.45, 0.46, 0.48, 0.48]
        f = interp1d(xx, yy, kind='cubic')
        bb.x0 += 0.5 + (bb.x1 - bb.x0) * math.pow(num_cols, f(num_cols))
        legend.set_bbox_to_anchor(bb, transform=ax.transAxes)
    elif obj.horizontal_legend:
        num_cols = obj.ncol if obj.ncol != None else len(obj.column_headers)
        if obj.legend_loc != None:
            legend = plt.legend(ncol=num_cols, loc=obj.legend_loc, fontsize=obj.fontsize)
        else:
            legend = plt.legend(ncol=num_cols, fontsize=obj.fontsize)
    elif obj.legend_loc != None:
        legend = plt.legend(loc=obj.legend_loc, fontsize=obj.fontsize)
    else:
        legend = plt.legend(fontsize=obj.fontsize)

    legend.get_frame().set_alpha(0)
    plt.tick_params(labelleft=True, labelright=True)
    if obj.xnbins != None:
        ax.locator_params(nbins=obj.xnbins, axis='x')
    if obj.ynbins != None:
        ax.locator_params(nbins=obj.ynbins, axis='y')

    if obj.yformat != None:
        ticks_loc = ax.get_yticks().tolist()
        ax.yaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        i = 0
        while i < len(ticks_loc) and ticks_loc[i] <= obj.ymax:
            i += 1
        if i > 0:
            i -= 1
        ticks_loc[i] = formatWithSpecifiedFormatStr(ticks_loc[i], obj.yformat)
        ax.set_yticklabels(ticks_loc)

    plt.savefig(obj.output, dpi=400, bbox_inches = 'tight', pad_inches = 0)
    plt.close()


